package demo.servicemix.camel;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class CamelActivator  implements BundleActivator {
	private CamelContext camelContext = new DefaultCamelContext();
	
	@Override
	public void start(BundleContext context) throws Exception {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("failover:(tcp://192.168.190.11:61616,tcp://192.168.190.12:61616)?backup=true&timeout=5000&randomize=false&jms.useAsyncSend=true");
		camelContext.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
		camelContext.addRoutes(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				from("jms:queue:message").filter(header("type").isEqualTo("cmd")).to("jms:queue:command");
				from("jms:queue:result").choice()
										.when(header("code").isEqualTo("1")).to("jms:queue:success")
										.when(header("code").isEqualTo("0")).to("jms:queue:error");
			}
		});
		
		camelContext.start();
		System.out.println("start camel context");
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		camelContext.stop();
		System.out.println("stop camel context");
	}

}
