package demo.servicemix.cxf;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "Book", namespace="http://www.demo.com/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {

	private int id;

	private String name;

	private String author;

	@XmlElement(name = "update_date")
	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	private Date updateDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Book[id=" + id + ", name=" + name + ", author=" + author + ", update_date=" + updateDate + "]";
	}
}
