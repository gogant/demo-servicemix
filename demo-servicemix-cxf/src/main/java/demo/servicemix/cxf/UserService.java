package demo.servicemix.cxf;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class UserService {

	@WebMethod
	public boolean login(String user, String passwd) {
		if(user == null || user.length()==0 || passwd == null || passwd.length() ==0){
			throw new IllegalArgumentException("用户名和密码不能为空！");
		}
		System.out.println("user login: user=" + user + ", passwd=" + passwd);
		if("hzp".equals(user) && "123".equals(passwd)){
			return true;
		} else {
			return false;
		}
	}
}
