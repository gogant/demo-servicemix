package demo.servicemix.proxy;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ProxyProcessor implements Processor {

	public ProxyProcessor() {
		System.out.println("CamelProcessor inited");
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		exchange.getIn().setHeader("SOAPAction", "");
	}

}
