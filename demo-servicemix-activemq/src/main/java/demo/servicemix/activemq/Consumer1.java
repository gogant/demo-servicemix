package demo.servicemix.activemq;

import javax.jms.Message;
import javax.jms.MessageConsumer;

public class Consumer1 extends MQContext {

	public Consumer1() {
		super(MQContext.jmsUrl_11,"karaf","karaf");
	}

	public void consumeForQueue(String queueName) throws Exception {
		try {
			this.init();
			MessageConsumer consumer = createQueueConsumer(queueName);
			Message message = null;
			int count = 0;
			while ((message = consumer.receive()) != null) {
				 System.out.println("consumer[1] receive :" + message);
//				if ((++count) % 100 == 0) {
//					System.out.println("consumer[1] receive count: " + count);
//				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			destroy();
		}
	}

	public void consumeForTopic(String topicName) throws Exception {
		try {
			this.init();
			MessageConsumer consumer = createTopicConsumer(topicName);
			Message message = null;
			int count = 0;
			while ((message = consumer.receive()) != null) {
				 System.out.println("consumer[1] receive :" + message);
//				if ((++count) % 100 == 0) {
//					System.out.println("consumer[1] receive count: " + count);
//				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			destroy();
		}
	}

	public static void main(String[] args) throws Exception {
//		new Consumer1().consumeForQueue("hello");
		new Consumer1().consumeForTopic("news");
	}
}
