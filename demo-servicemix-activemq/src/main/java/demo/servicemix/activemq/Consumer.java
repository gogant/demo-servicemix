package demo.servicemix.activemq;

import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;

public class Consumer extends MQContext {

	public Consumer() {
		super(jmsUrl_all,"karaf","karaf");
	}
	
	public void forQueueListener (String queueName) throws Exception {
		try {
			init();
			MessageConsumer consumer = createQueueConsumer(queueName);
			consumer.setMessageListener(new MessageListener(){
				public void onMessage(Message message) {
					System.out.println(message);
				}
			});
			Thread.sleep(100000);
		} catch (Exception e) {
			throw e;
		} finally {
			destroy();
		}
	}
	
//	public void forQueueListenerLambda (String queueName) throws Exception {
//		try {
//			init();
//			MessageConsumer consumer = createQueueConsumer(queueName);
//			consumer.setMessageListener(message -> {
//				System.out.println(message);
//			});
//			Thread.sleep(100000);
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			destroy();
//		}
//	}
	
	public static void main(String[] args) throws Exception{
		new Consumer().forQueueListener("hello");
	}

}
