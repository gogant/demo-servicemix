package demo.servicemix.activemq;

import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;

public class CamelTest extends MQContext {

	public CamelTest() {
		super(jmsUrl_all);
	}

	public void doSendMesssage() {
		try {
			init();
			MessageProducer producer = createQueueProducer("message");
			int count = 10;
			while (count-- > 0) {
				Message msg = createTextMessage("hello world");
				msg.setStringProperty("type", count % 2 == 0 ? "cmd" : "notice");
				producer.send(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			destroy();
		}
	}

	public void doProcessMesssage() {
		try {
			init();
			MessageConsumer consumer = createQueueConsumer("command");
			MessageProducer producer = createQueueProducer("result");
			Message msg = null;
			int count = 0;
			while ((msg = consumer.receiveNoWait()) != null) {
				System.out.println(msg);
				msg = createTextMessage("process done");
				count++;
				msg.setStringProperty("code", String.valueOf(count % 2));
				producer.send(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			destroy();
		}
	}

	public static void main(String[] args) {
		CamelTest test = new CamelTest();
		test.doSendMesssage();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
		}
		test.doProcessMesssage();
	}

}
