package demo.servicemix.activemq;

import javax.jms.MessageProducer;

public class Producer extends MQContext {

	public Producer() {
		super(jmsUrl_all,"karaf","karaf");
	}
	
	public void producerForQueue(String queueName, int count) throws Exception {
		try {
			init();
			MessageProducer producer = createQueueProducer(queueName);
			while (count-- > 0) {
				// System.out.println("send " + count);
				producer.send(createTextMessage("hello : " + count));
			}
			System.out.println("send done");
		} catch (Exception e) {
			throw e;
		} finally {
			destroy();
		}
	}

	public void producerForTopic(String topicName, int count) throws Exception {
		try {
			init();
			MessageProducer producer = createTopicProducer(topicName);
			while (count-- > 0) {
				// System.out.println("send " + count);
				producer.send(createTextMessage("hello : " + count));
			}
			System.out.println("send done");
		} catch (Exception e) {
			throw e;
		} finally {
			destroy();
		}
	}

	public static void main(String[] args) throws Exception {
//		new Producer().producerForQueue("hello", 1000);
		new Producer().producerForTopic("news", 10);
	}
}
