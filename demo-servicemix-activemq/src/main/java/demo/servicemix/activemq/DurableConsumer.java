package demo.servicemix.activemq;

import javax.jms.Message;
import javax.jms.MessageConsumer;

public class DurableConsumer extends MQContext {
	public DurableConsumer() {
		super(jmsUrl_all,"karaf","karaf");
	}

	public void durableSubscribe(String topicName) throws Exception {
		try {
			init("client@190.1");
			MessageConsumer consumer = createDurableSubscribe(topicName, "hzp", null);
			Message message = null;
			while ((message = consumer.receive()) != null) {
				System.out.println(message);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			destroy();
		}
	}

	public static void main(String[] args) throws Exception {
		new DurableConsumer().durableSubscribe("news");
	}

}
