package demo.servicemix.osgi;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

public class MainActivator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		context.addBundleListener(new BundleListener() {
			@Override
			public void bundleChanged(BundleEvent event) {
				System.out.println(event);
			}
		});
		context.addServiceListener(new ServiceListener() {
			public void serviceChanged(ServiceEvent event) {
				System.out.println(event);
			}
		});
		System.out.println(context);
		System.out.println("bundle base path: "+context.getDataFile(""));
		System.out.println("start my bundle v0.0.2");
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("stop my bundle v0.0.2");
	}
}
